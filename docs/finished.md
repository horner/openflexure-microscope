# Assembly complete

Congratulations, you have now assembled your OpenFlexure Microscope.

## The completed microscope {pagestep}

![A render of the completed microscope](renders/complete_microscope_{{var_optics, default:rms}}0.png)
![A render of the completed microscope](renders/complete_microscope_{{var_optics, default:rms}}2.png)
![A render of the completed microscope](renders/complete_microscope_{{var_optics, default:rms}}3.png)
![A render of the completed microscope](renders/complete_microscope_{{var_optics, default:rms}}1.png)

Your completed microscope should now look like these pictures.  
There is also an [interactive 3D view](interactive_3d_view_{{var_optics, default:rms}}.md) of the finished microscope.

## Software set-up {pagestep}

Setting up the software is not yet included in these instructions.  It is detailed on the website, in the ["install" page].  Using the software is described in the ["control" page].

["install" page]: https://openflexure.org/projects/microscope/install
["control" page]: https://openflexure.org/projects/microscope/control

## Join the community {pagestep}

OpenFlexure is community project, and we are always delighted to hear from people who have built it on the [OpenFlexure forum](https://openflexure.discourse.group/). The forum is also a great resource for things to do with your microscope, tips and tricks, and others who have built it. Posting photos of your build, pictures taken with the microscope, or even just the fact that you've built it, will all help to keep the project thriving.
