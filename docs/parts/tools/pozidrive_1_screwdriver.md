# Pozidrive #1 screwdriver
---
PartData:
    Suppliers:
        RS Components:
            PartNo: 176-3436
            Link: https://uk.rs-online.com/web/p/screwdrivers/1763436
---

You will need a small screwdriver for use with the self tapping screws.  These are specified for PZ1 drive, but if your screws are different, you will need a correspondingly different screwdriver.