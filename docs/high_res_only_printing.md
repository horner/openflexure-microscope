{{include: inverted_only_printing.md}}
* [Optics module]{output,qty:1}: [optics_picamera_2_rms_f50d13.stl](models/optics_picamera_2_rms_f50d13.stl){previewpage} - **This must be printed in [black][Black PLA filament]{Qty: 50g}!** [i](info_pages/why_optics_black.md)
* [pi camera cover]{output,qty:1}: [picamera_2_cover.stl](models/picamera_2_cover.stl){previewpage}

You can [download all of the STLs as a single zipfile](high-res-stls.zip){zip, pattern:"*.stl"}

[Black PLA filament]: parts/materials/black_pla_filament.md "{cat:material}"